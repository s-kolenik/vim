"nastaveni kodovani
set encoding=utf-8

" nahrani pluginu
so ~/.vim/plugin_related.vim

" nahrani klavesovych zkratek
so ~/.vim/keyboard_mappings.vim

" urceni slozky pro ukladani swap souboru
set directory^=$HOME/.vim/swapsoubory/

" aktivni slozka, je ta, ve ktere je prave otevreny soubor
set autochdir

" umoznuje opustit zmeneny neulozeny soubor
set hidden
