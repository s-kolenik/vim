set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=~/.fzf
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-surround'
Plugin 'vim-scripts/AutoComplPop'
Plugin 'tommcdo/vim-exchange'

Plugin 'junegunn/fzf.vim'
nmap <A-:> :Buffers<CR>
nmap <A-F> :GFiles<CR>
nmap <A-L> :Lines<CR>

Plugin 'preservim/nerdtree'
nnoremap <A-t> :NERDTreeToggle<Cr>

Plugin 'jiangmiao/auto-pairs'
let g:AutoPairsShortcutToggle = '<A-A>'
let g:AutoPairsShortcutJump = ''
let g:AutoPairsMoveCharacter = ''

Plugin 'easymotion/vim-easymotion'
nmap <A-w> <Plug>(easymotion-w)
nmap <A-q> <Plug>(easymotion-b)
nmap <A-j> <Plug>(easymotion-j)
nmap <A-k> <Plug>(easymotion-k)
map <Leader> <Plug>(easymotion-prefix)

Plugin 'SirVer/ultisnips'
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
