" vzhled
      " nastaveni barev, upravene schema slate
      " soubor ~/.vim/colors/slatecp.vim
colorscheme slatecp
set guifont=Monospace\ 10
set relativenumber
set cursorline
      " horizontalni kurzor v normal modu, visual modu a command-line modu
set guicursor+=n-v-c:hor15-Cursor/lCursor-blinkwait1400-blinkon650-blinkoff500
      " vertikalni kurzor v insert modu a comman-line insert modu
set guicursor+=i-ci:ver22-Cursor/lCursor-blinkwait400-blinkon650-blinkoff500
      " neni menu, right-hand srollbar, left-hand scrollbar, toolbar
      " (pro vice informaci :help go-m, :help go-r, ...)
set guioptions-=mrLtT
" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
" odsazeni
set autoindent
set nocindent
" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
" tab
command! Tab2 set expandtab shiftwidth=2 tabstop=2 softtabstop=2
command! Tab4 set noexpandtab shiftwidth=4 tabstop=4 softtabstop=4
Tab2
" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
" cas na expanzi znaku (napr. (...) vs () )
set timeoutlen=800
" ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
" zabranuje automatickemu odsazovani a formatovani komentaru
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
